{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE FlexibleInstances #-}
module Main where

import Data.Aeson
import Lucid
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Servant.HTML.Lucid
import GHC.Generics

type API = "hello" :> Capture "name" String :> Get '[PlainText] String
      :<|> "lists" :> Get '[JSON, HTML] [List]
      :<|> "list"  :> Capture "id" Integer :> Get '[JSON, HTML] [String]

data List = List { listName :: String, listItems :: [String] }
            deriving Generic

instance ToJSON List

instance ToHtml [List] where
  toHtml = undefined

instance ToHtml [String] where
  toHtml = undefined

exampleLists :: [(Integer, List)]
exampleLists = [ (1, List "Supermarket" ["Milk","Eggs"])
               , (2, List "Gym" ["Run", "Weight"])
               ]

srv :: Server API
srv = hello :<|> lists :<|> list
  where hello :: String -> Handler String
        hello name = return $ "Hello, " ++ name ++ "!"
        lists :: Handler [List]
        lists = return (map snd exampleLists)
        list :: Integer -> Handler [String]
        list id = let Just l = lookup id exampleLists
                   in return (listItems l)

main :: IO ()
main = run 8080 $ serve (Proxy :: Proxy API) srv
