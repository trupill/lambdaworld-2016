import Html exposing (..)
import Http
import Json.Decode as Json
import List
import Task

type alias ListItem = (String, Bool)
type Model = Loading
           | ErrorLoading
           | Loaded (List ListItem)

url : String
url = "https://api.myjson.com/bins/3qg7q"

initial : (Model, Cmd Message)
initial = (Loading, Task.perform FetchError FetchOK (Http.get decoder url))

decoder : Json.Decoder (List ListItem)
decoder = Json.list
  (Json.map2 (,) (Json.field "text" Json.string) (Json.field "done" Json.bool))

type Message = FetchError Http.Error
             | FetchOK (List ListItem)

update : Message -> Model -> (Model, Cmd Message)
update msg _ = case msg of
  FetchError _ -> (ErrorLoading, Cmd.none)
  FetchOK lst  -> (Loaded lst, Cmd.none)

view : Model -> Html Message
view mdl = case mdl of
  Loading      -> text "Loading"
  ErrorLoading -> text "Error loading"
  Loaded lst   -> ul [] (List.map (\(x,y) -> li [] [text x]) lst)

main = program { init = initial, view = view, update = update, subscriptions = \_ -> Sub.none }