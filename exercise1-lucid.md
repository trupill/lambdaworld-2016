# Exercise 1 - HTML with Lucid

JSON is widely used as an interchange format, but by no means users can directly interface with it. Instead (or in addition to) a JSON representation, you can return (plain old) HTML (did I use enough parenthesis in this sentence?).

## Declaring HTML in your endpoints

In order to make Servant respond with HTML data, you just need to add that content type to the `GET` list. The result, in our simple example of to-do lists, is changd into:

```haskell
type API = "lists" :> Get '[JSON, HTML] [List]
      :<|> "list"  :> Capture "id" Integer :> Get '[JSON, HTML] [String]
```

No change is needed to the `Server API` value! Remember that when you serve an API, you just need to return a value whose type is given in the second argument to `Get`; in this case either `[List]` or `[String]`. Since we are not changing that argument, but the first, `srv` can stay exactly the same.

## Conversion to HTML

Of course, we cannot expect to just add `HTML` to the list of content types and go away with a server which responds with the HTML you want. Indeed, if you try to compile the code as is, you will get several errors about missing `ToHtml` instances. This is the counterpart of `ToJson` for HTML data: it must be implemented by every data type for which we accept the HTML content type.

Before moving on, let me warn about a small change we need to do at the top of the file. Declaring an instance like:

```haskell
instance ToHtml [List] where
  toHtml l = ...
```

is not allowed in Haskell 98, since the type specified in the declaration, namely `[List]` is not of the form type constructor + variables. Instead we have a type constructor, `[]`, applied to a constant type, `List`. In order to lift this restriction we need to enable the `FlexibleInstances` extension. But be careful, if you have an instance for `[a]` with a variable `a`, these instances will overlap with them.

Going back to the main theme of this exercise, we were in the middle of implementing the `toHtml` method. In order to do so we are going to use the `lucid` library, which provides combinators for describing and constructing HTML documents. The design of this library is very simple: for each HTML element there is a corresponding function. Usually, the name of the function is the name of the element followed by an underscore. By nesting function applications, HTML structure can be described. A very simple example is simply some text in bold-face:

```haskell
b_ "Hello"
```

This is enough until the point in which you need to nest more than one element inside another. One example of where this functionality is required is lists.

```html
<ul>
  <li>Element 1</li>
  <li>Element 2</li>
</ul>
```

In this case, we can make use of the fact that HTML documents as represented by `lucid` form a monad! Albeit a special one, since in this case we are never interested on the return value of `Html`, but just in the tree structure of the computation. The translation of the previous chunk of HTML is:

```html
ul_ (do li_ "Element 1"
        li_ "Element 2")
```

The great advantage of this representation, pioneered by the `blaze-html` library, is that you can reuse monadic combinators to describe HTML documents. Haskell is the ultimate templating language! For example, instead of duplicating the call to `li_` in the previous code, we could have mapped it over the list of elements:

```haskell
ul_ (mapM_ li_ ["Element 1", "Element 2"])
```

As a final note, you might need to specify some attributes of a HTML node, like its `class`. This information is passed withing a list as first argument to the element.

```haskell
p_ [class_ "title"] "Hello"
```

How to make `p_` to accept these two kinds of arguments involves some type magic which unfortunately we don't have time to describe now. If you are interested, the documentation of the `Lucid` module describes the technique in depth.

## Your task

Fill in the conversion of `[List]` and `[String]` to HTML in the given template. You can return very simple HTML or use your full creativity. Unfortunately, `lucid` does not provide simple access to the `<blink>` tag :(