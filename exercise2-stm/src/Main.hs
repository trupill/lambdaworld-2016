{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
module Main where

import Control.Concurrent.STM
import Control.Monad.IO.Class
import Network.Wai
import Network.Wai.Handler.Warp
import Servant

type API = "value" :> Get '[JSON] Integer
      :<|> "increment" :> Capture "id" Integer :> Get '[JSON] Integer

srv :: TVar Integer -> Server API
srv counter = value :<|> increment
  where value :: Handler Integer
        value = liftIO . atomically $ readTVar counter
        increment :: Integer -> Handler Integer
        increment n = liftIO . atomically $ do
                        modifyTVar counter (+n)
                        readTVar counter

main :: IO ()
main = do
  counter <- newTVarIO 0
  run 8080 $ serve (Proxy :: Proxy API) (srv counter)
