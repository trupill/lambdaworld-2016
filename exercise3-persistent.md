# Exercise 3 - Database

This exercise helps you in getting more acquainted with the Persistent library. The basis for the exercise is the To-Do lists web application that we have been looking at so far: your goal is to code some extra routes. The [documentation](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html) for the `persistent` library will come handy in these tasks.

### Task 1 - Create a new list

Up to now you are able to add new items to an already existing list, but not to create a new list itself. Add a new route `PUT /list` which receives a list name in its request body and creates such a list in the database. The response such be a JSON representation of the newly created list.

### Task 2 - Add pagination to list items

Right now we have a `GET /list/<list id>/items` route which returns all the elements in a given to-do list. This list, however, may grow very big. Implement a version of this route suitable for pagination. That is, including extra parameters which tell the initial offset and the number of records to return from the query. As before, a wrong list identifier should return the error code 404.

### Task 3 - Show all list items, including the asssociated list

The route `GET /all/items` should return the information from all list items in the system. The result must be a list of elements of the form:

```
{ "id" : 1
, "text" : "Do something"
, "done" : false
, "list" : { "id" : 2, "name" : "To do this fall" } }
```

Note that a `ListItem` value only stores the key to a `List`, not the entire value. Thus, you need to recover that information using extra queries. The following function will be helpful in treating lists in a monadic way:

```haskell
mapM :: Monad m => (a -> m b) -> [a] -> m [b]
```

**Extra**: if you think that Persistent does not give you the right tools for a performant implementation, then you are right: Persistent does not allow doing joins in the database. For information about how to solve this problem, look at exercise 4.

### Task 4 - Change the `done` field

Right now all list items have a `done` field which is always set to `False`. We want to add a route `POST /list/<list id>/item/<item id>/completed` which shall change this state. Note that you need to check that the list item exists and is inside the mentioned list before changing the completion status.

In order to implement this state change, Persistent gives you two different options. My suggestion is to write both pieces of code, and compare them in terms of readability and performance:

* The first option is using the [`replace`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:replace) function. The replacement operation takes a key in the database and the new value which should be associated to that key. That is, you need to obtain the information for the entire list item, and then perform a replacement using the same information with the corresponding field changed.
* Another possibility is using [`update`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:update). In this case, instead of completely new value, you tell Persistent which are the updates to be done in the record which matches a given key.

### Task 5 - Remove an entire list

Some people prefer to delete the to-do lists once all the elements have been completed. You need to implement a `DELETE /list/<list id>` route to do so. But of course you do not want orphan items, so you first need to remove all of them too. In this task you are asked to do this in no less than three different ways:

* The first one is doing a query to get all items from a given list, and then call [`delete`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:delete) on each of them. Note that, given that `delete` only requires the identifier of the task to delete, you can change from using `selectList` to [`selectKeys`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:selectKeys).
* Anther possibility is to delete all the item from a given list in bulk, by means of the [`deleteWhere`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:deleteWhere) function. This function is similar to `selectList`, but instead of returning the records that match the data, it deletes them.
* Finally, you may perform a *cascade delete*, which removes a value, in this case the list, with all the associated data from other entities. The corresponding function in Persistent is [`deleteCascade`](https://www.stackage.org/haddock/lts-10.3/persistent-2.7.1/Database-Persist-Class.html#v:deleteCascade).