import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List

type alias ListItem = (String, Bool)
type Model = Show (List ListItem)
        -- | more here

type Message = Delete Int
             | ToggleComplete Int
          -- | more here

but : List a -> Int -> List a
but lst n = List.append (List.take n lst) (List.drop (n+1) lst)

mapAt : (a -> a) -> List a -> Int -> List a
mapAt f lst n = case lst of
  []      -> []
  x :: xs -> case n of
    0 -> f x :: xs
    _ -> x :: mapAt f lst (n-1)

update : Message -> Model -> Model
update msg (Show lst) = case msg of
  Delete n -> Show (but lst n)
  ToggleComplete n -> Show (mapAt (\(t, c) -> (t, not c)) lst n)

view : Model -> Html Message
view (Show lst) = ul [] (List.map2 viewListItem (List.range 0 (List.length lst - 1)) lst)

viewListItem : Int -> ListItem -> Html Message
viewListItem ix (item, compl)
  = li [] [ text item
          , text " "
          , button [onClick (Delete ix)] [text "Delete"]
          ]

initial : Model
initial = Show [ ("Mark as completed", False)
               , ("Add new item", False) ]

main = beginnerProgram { model = initial, view = view, update = update }