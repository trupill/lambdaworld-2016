# Strongly-Typed Web Applications with Haskell and Elm
## Proudly presented at [LambdaWorld 2016](http://lambda.world)

Here are the companion files to the web applications workshop presented at LambdaWorld 2016 by Alejandro Serrano. They include example applications plus exercises statements and templates:

1. [HTML with Lucid](https://gitlab.com/trupill/lambdaworld-2016/blob/master/exercise1-lucid.md)
2. [Software Transactional Memory (STM)](https://gitlab.com/trupill/lambdaworld-2016/blob/master/exercise2-stm.md)
3. [Persistent for databases](https://gitlab.com/trupill/lambdaworld-2016/blob/master/exercise3-persistent.md)
4. [Esqueleto for databases and joins](https://gitlab.com/trupill/lambdaworld-2016/blob/master/exercise4-esqueleto.md)
5. [To-Do List with Elm](https://gitlab.com/trupill/lambdaworld-2016/blob/master/exercise5-elm.md)