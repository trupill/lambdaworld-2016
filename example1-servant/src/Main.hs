{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import Data.Aeson
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import GHC.Generics

type API = "hello" :> Capture "name" String :> Get '[PlainText] String
      :<|> "lists" :> Get '[PlainText] String
      :<|> "list"  :> Capture "id" Integer :> Get '[PlainText] String
      :<|> "listsj" :> Get '[JSON] [List]
      :<|> "listj"  :> Capture "id" Integer :> Get '[JSON] [String]

data List = List { listName :: String, listItems :: [String] }
            deriving Generic

{-
instance ToJSON List where
  toJSON (List { .. })
    = object [ "name"  .= toJSON listName
             , "items" .= toJSON listItems ]
-}

instance ToJSON List

exampleLists :: [(Integer, List)]
exampleLists = [ (1, List "Supermarket" ["Milk","Eggs"])
               , (2, List "Gym" ["Run", "Weight"])
               ]

srv :: Server API
srv = hello :<|> lists :<|> list :<|> listsj :<|> listj
  where hello :: String -> Handler String
        hello name = return $ "Hello, " ++ name ++ "!"
        lists :: Handler String
        lists = return $ show [listName l | (_,l) <- exampleLists]
        list :: Integer -> Handler String
        list id = let Just l = lookup id exampleLists
                   in return $ show (listItems l)
        listsj :: Handler [List]
        listsj = return (map snd exampleLists)
        listj :: Integer -> Handler [String]
        listj id = let Just l = lookup id exampleLists
                    in return (listItems l)

app :: Application
app = serve (Proxy :: Proxy API) srv

main :: IO ()
main = run 8080 app
