import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type alias Model = String

initial : Model
initial = ""

type Message = NewName String

update : Message -> Model -> Model
update (NewName n) _ = n

view : Model -> Html Message
view model = div []
                 [ input [ placeholder "Your name here"
                         , onInput NewName
                         , value model ] []
                 , div [] [ case model of
                              "" -> text "I don't know who you are"
                              _  -> text ("Hello, " ++ model ++ "!") ]
                 ]

main = beginnerProgram { model = initial, view = view, update = update }