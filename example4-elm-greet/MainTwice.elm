import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type alias Model = String
initial : Model
initial = ""
type Message = NewName String
update : Message -> Model -> Model
update (NewName n) _ = n
view : Model -> Html Message
view model = div []
                 [ input [ placeholder "Your name here"
                         , onInput NewName
                         , value model ] []
                 , div [] [ case model of
                              "" -> text "I don't know who you are"
                              _  -> text ("Hello, " ++ model ++ "!") ]
                 ]

type alias Model2 = { name1 : Model, name2 : Model }

initial2 : Model2
initial2 = { name1 = "", name2 = "" }

type Message2 = NewName1 Message | NewName2 Message

update2 : Message2 -> Model2 -> Model2
update2 msg mdl = case msg of
  NewName1 n -> { mdl | name1 = update n mdl.name1 }
  NewName2 n -> { mdl | name2 = update n mdl.name1 }

view2 : Model2 -> Html Message2
view2 m = div [] [ Html.map NewName1 (view m.name1)
                 , Html.map NewName2 (view m.name2) ]

main = beginnerProgram { model = initial2, view = view2, update = update2 }