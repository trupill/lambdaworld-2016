{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad.IO.Class
import Control.Monad.Logger
import Data.Pool
import Database.Persist
import Database.Persist.Sql
import qualified Database.Persist.Sqlite as Sqlite
import Network.Wai.Handler.Warp
import Servant
import Data.Validator

import Db

type API = "lists" :> Get '[JSON] [List]
      :<|> "list"  :> Capture "id" Integer
                   :> Get '[JSON] List
      :<|> "list"  :> Capture "id" Integer
                   :> "items"
                   :> Get '[JSON] [ListItem]
      :<|> "list"  :> Capture "id" Integer
                   :> "items"
                   :> ReqBody '[PlainText] String
                   :> Put '[JSON] Bool

srv :: Pool SqlBackend -> Server API
srv dbPool = lists :<|> list :<|> listItems :<|> newItem
  where withDb f = liftIO $ runSqlPersistMPool f dbPool
        lists :: Handler [List]
        lists =
          do ls <- withDb $ selectList [] [Asc ListBefore]
             return (map entityVal ls)
        list :: Integer -> Handler List
        list id = 
          do let lId = ListKey (SqlBackendKey $ fromInteger id)
             l <- withDb $ get lId
             case l of
               Nothing -> throwError err404
               Just l' -> return l'
        listItems :: Integer -> Handler [ListItem]
        listItems id =
          do let lId = ListKey (SqlBackendKey $ fromInteger id)
             l <- withDb $ get lId
             case l of
               Nothing -> throwError err404
               Just _ -> do is <- withDb $ selectList [ListItemList ==. lId] []
                            return $ map entityVal is
        {- newItem :: Integer -> String -> Handler Bool
        newItem id text =
          do let lId = ListKey (SqlBackendKey $ fromInteger id)
             l <- withDb $ get lId
             case l of
               Nothing -> throwError err404
               Just _ -> -- First version
                         case runValidator (minLength 1 "Mandatory text") text of
                           Left msg -> return False
                           Right _  -> do i <- withDb $ insertUnique (ListItem text False lId)
                                          return $ maybe False (const True) i -}
        newItem :: Integer -> String -> Handler Bool
        newItem id text =
          do let lId = ListKey (SqlBackendKey $ fromInteger id)
             l <- withDb $ get lId
             case runValidator validator (text, lId, l) of
               Left msg   -> throwError err403 { errBody = msg }
               Right item -> do i <- withDb $ insertUnique item
                                return $ maybe False (const True) i
          where validator (text, lstId, lst) =
                  ListItem <$> (minLength 1 "Mandatory text"
                                >=> maxLength 140 "No longer than a tweet") text
                           <*> pure False
                           <*> (const lstId <$> requiredValue "Cannot find list" lst)


main :: IO ()
main = do
  -- Create the database
  Sqlite.runSqlite "example.db" $ Sqlite.runMigration migrateAll
  -- Initialize the connection pool
  runNoLoggingT $ Sqlite.withSqlitePool "example.db" 10 $ \dbPool ->
    NoLoggingT $ run 8080 $ serve (Proxy :: Proxy API) (srv dbPool)
      
       
