{-# LANGUAGE TemplateHaskell, QuasiQuotes, TypeFamilies, MultiParamTypeClasses,
             EmptyDataDecls, FlexibleContexts, FlexibleInstances,
             GADTs, GeneralizedNewtypeDeriving #-}
module Db where

import Data.Time
import Database.Persist.TH

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
List json
  name   String
  before UTCTime Maybe
  deriving Show
ListItem json
  text String
  done Bool
  list ListId
  UniqueItem text list
  deriving Show
|]