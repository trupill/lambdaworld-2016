{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
module Main where

import Network.Wai
import Network.Wai.Handler.Warp
import Servant

type API = "hello" :> Capture "name" String :> Get '[PlainText] String

srv :: String -> Handler String
srv name = return $ "Hello, " ++ name ++ "!"

app :: Application
app = serve (Proxy :: Proxy API) srv

main :: IO ()
main = run 8080 app
