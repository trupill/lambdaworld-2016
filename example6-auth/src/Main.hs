{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE TypeOperators   #-}
module Main where

import Data.ByteString.Char8 (unpack)
import Data.Char
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Servant.API.BasicAuth

type API = "hello" :> Capture "name" String :> Get '[PlainText] String
      :<|> "bye" :> BasicAuth "lambda-world" User :> Get '[PlainText] String

data User = User { nick :: String }

auth :: BasicAuthCheck User
auth = BasicAuthCheck $ \(BasicAuthData name password) ->
         let (name', password') = (unpack name, unpack password)
          in if password' == map toUpper name'
                then return $ Authorized (User name')
                else return Unauthorized

srv :: Server API
srv = (\name -> return $ "Hello, " ++ name ++ "!")
 :<|> (\user -> return $ "Bye bye, " ++ nick user ++ "!")

main :: IO ()
main = run 8080 $
         serveWithContext (Proxy :: Proxy API)
                          (auth :. EmptyContext)
                          srv
