# Exercise 5 - To-Do List with Elm

In this exercise we are going to revisit the to-do list web application, but without caring about communication with the server. The goal is for you to get used to the Elm Architecture, by adding new functionality to a provided template.

## Current implementation

Right now the model and the messages are quite simple. The model just holds a list of items, and the possible messages which `update` can handle either mark (or unmarks) an item as completed, or removes the item entirely.

```haskell
type alias ListItem = (String, Bool)
type Model = Show (List ListItem)

type Message = Delete Int
             | ToggleComplete Int
```

Both messages have as argument the position of the item to modify, so that the `update` function can target the correct one. Thus, in the `view` function we must take care that these indices are correctly given. We do so by zipping a list of indices with the list of items by means of the `map2` function, which corresponds to Haskell's `zipWith`.

```haskell
List.map2 viewListItem [0 .. (List.length lst - 1)] lst
```

## Task 1: mark as completed

In the current state, the template implements the update for both marking as completed and deleting, but only the latter can be accessed by the user via a corresponding button. Your first task is to modify the view so that the user can mark and unmark an item as completed. You can do this in two different ways:

* Using another button in addition to _Delete_. In this case, you should ensure that the button text changes to _Mark as completed_ or _Mark as pending_ depending on the current state of the item.
* Instead of using a `li` element, you can use a checkbox input. The [documentation of the `onCheck` function](http://package.elm-lang.org/packages/elm-lang/html/1.1.0/Html-Events#onCheck) could be useful.

## Task 2: add a new item

Right now the application has a big shortcoming: you can remove items but not add new ones! The second task of this exercise is to implement the new functionality. There are increasingly complex ways in which you could handle it:

* As a first step, just include a text field and a button which are always present, and keep track of the current state of the text field. When the user clicks the _Add_ button, create a new item at the end of the list, and clean the text in the input field.
* As a second step, we would like to hide the functionality of adding until the user clicks a _New item_ button. In this case, you state needs to be enlarge with different alternatives, depending on whether the new item part should be shown or not.
* Finally, we can make the _New item_ window modal, so that the user cannot escape without giving us a new item text or cancelling the process. I suggest trying the [`elm-dialog`](http://package.elm-lang.org/packages/krisajenkins/elm-dialog/latest) package. /Disclaimer:/ I haven't done a thorough experimentation with this package, so it may not completely suit the needs of the application.

The main takeaway from this second task is that in the Elm Architecture your model and messages completely encapsulate all the states and changes that may occur during the lifetime or the application. In some sense, they describe your application as a state machine. As usual, this comes with a trade-off: the application is easy to understand, since it has no hidden parts or state. But at the same time, we need to be completely explicit: adding a feature may involve rethinking the representation of the model.